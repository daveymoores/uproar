// Foundation JavaScript
// Documentation can be found at: http://foundation.zurb.com/docs
$(document).foundation();


$(document).ready(function(){

	//sticky aside
	var stik = $('#roarLeftColumn').find('.sticky-wrapper');
	var $window = $(window); 
	var stiksize = stik.width();
	var changeWidth = $('#uproarsticker').width();

	$window.resize(function(i){
		var el = stik.width();

		$('#uproarsticker').css('width', stiksize);
	});


	$("footer").waypoint(function(direction){

		if(direction == 'down') {
			var stopHeight = $('.roarBodyWrapper').height() - $('#uproarsticker').height() - 330;
		    $('#uproarsticker').css({
		        position: "absolute",
		        top: stopHeight,
		        width: changeWidth
		    });
		} else {
		     $('#uproarsticker').removeAttr('style').css('width', changeWidth);
		}
	},{
	    offset: 446//$('#uproarsticker').height()//height of footer elements
	});


	var windowWidth = $window.width();

	if(windowWidth > 1024) {

		var waypoint = new Waypoint.Sticky({
		  element: document.getElementById('uproarsticker'),
		  handler: function(direction) {

		  	var stik = $('#roarLeftColumn').find('.sticky-wrapper');
		  	var stiksize = stik.width();


		    $('#uproarsticker').css('width', stiksize);

		    if (direction == 'down') {

		    	$('#logoHook').velocity({
		    		height : '75px'
		    	}, 350, 'swing', function(){
		    		
		    	});

		    } else if (direction == 'up') {

		    	$('#logoHook').velocity({
		    		height : '0px'
		    	}, 250, 'swing');

		    }
		  }
		});

	}
	//end sticky aside


	//parallax code
	var $el = $('.imageContainer');

	
	$('.imageContainer[data-type="background"]').each(function(i){
        var $bgobj = $(this); // assigning the object

        var unitHeight = ($(this).height()*i);

    
        $(window).scroll(function() {
            
            if(i == 0) {
            	var yPos = -($window.scrollTop() / $bgobj.data('speed'));
            } else if(i>0) {
            	var elTop = $el.eq(i-1).offset().top + $bgobj.height(),
            		distance = ($window.scrollTop() - elTop),
            	    yPos = -(distance / $bgobj.data('speed'));
            	                	
            }
            
            // Put together our final background position
            var coords = '110% '+ yPos + 'px';

            // Move the background
            $bgobj.css({ backgroundPosition: coords });
        }); 
    });    
	//end parallax code



	if(windowWidth > 640) {

		//hover
		$('#roarBody').find('.hoverOverlay').mouseenter(function(){
			
			var $obj = $(this),
				$objpar = $obj.parent(),
				time = 150;

			$obj.parent().find('.roarOverlay').stop().velocity({
				opacity : 1
			}, time, 'swing');

			$obj.parent().find('.floatTitle').stop().velocity({
			    backgroundColor: "#ffffff",
			    color: "#000000"
			}, time, 'swing');

			$obj.parent().find('span').stop().velocity({
			    opacity: 1,
			    marginTop: '-15px'
			}, time, 'swing');

			$obj.parent().find('a').stop().velocity({
			    opacity: 1,
			    marginTop: '-5px'
			}, time, 'swing');
		
		});

		$('#roarBody').find('.hoverOverlay').mouseleave(function(){
			
			var $obj = $(this),
				$objpar = $obj.parent(),
				time = 400;

			$objpar.find('.roarOverlay').stop().velocity({
				opacity : 0
			}, time, 'swing');

			$objpar.find('.floatTitle').stop().velocity({
			    backgroundColor: "#000000",
			    color: "#ffffff"
			}, time, 'swing');

			$objpar.find('span').stop().velocity({
			    opacity: 0,
			    marginTop: '0px'
			}, time, 'swing');

			$objpar.find('a').stop().velocity({
			    opacity: 0,
			    marginTop: '0px'
			}, time, 'swing');
		
		});

	}


$(document).foundation({
    reveal : {
        animation_speed: 250,
        animation: 'fade'
    }
});


$('.hoverOverlay').on('click', function(){

	var proj = $(this).data('proj');
	console.log('This is the proj' + proj);
	var $modal = $('#firstModal');

	var array0 = ['img/screenshots/rpg-screens/screen_1.jpg', 'img/screenshots/rpg-screens/screen_2.jpg', 'img/screenshots/rpg-screens/screen_3.jpg', 'img/screenshots/rpg-screens/screen_4.jpg'];
	var array1 = ['img/screenshots/fitz-screens/screen_1.jpg', 'img/screenshots/fitz-screens/screen_2.jpg', 'img/screenshots/fitz-screens/screen_3.jpg', 'img/screenshots/fitz-screens/screen_4.jpg'];
	var array2 = ['img/screenshots/fitz-screens/screen_1.jpg', 'img/screenshots/fitz-screens/screen_2.jpg', 'img/screenshots/fitz-screens/screen_3.jpg', 'img/screenshots/fitz-screens/screen_4.jpg'];
	var array3 = ['img/screenshots/injustic-screens/screen_1.jpg', 'img/screenshots/injustic-screens/screen_5.jpg', 'img/screenshots/injustic-screens/screen_2.jpg', 'img/screenshots/injustic-screens/screen_3.jpg', 'img/screenshots/injustic-screens/screen_4.jpg'];
	var array4 = ['img/screenshots/fitz-screens/screen_1.jpg', 'img/screenshots/fitz-screens/screen_2.jpg', 'img/screenshots/fitz-screens/screen_3.jpg', 'img/screenshots/fitz-screens/screen_4.jpg'];
	var array5 = ['img/screenshots/fitz-screens/screen_1.jpg', 'img/screenshots/fitz-screens/screen_2.jpg', 'img/screenshots/fitz-screens/screen_3.jpg', 'img/screenshots/fitz-screens/screen_4.jpg'];
	var array6 = ['img/screenshots/timico-screens/screen_1.jpg', 'img/screenshots/timico-screens/screen_2.jpg', 'img/screenshots/timico-screens/screen_3.jpg'];


	switch(proj) {
	    case 0:

	    	var arrayLength0 = array0.length;

	    	for(var i=0 ; i<arrayLength0 ; i++) {

	        	$modal.find('#roarCarousel').append('<div class="fade-in" data-src="' + array0[i] + '"></div>');
	       		$modal.find('.skills').text('CSS, HTML5, ANGULARJS, NODE JS, RAILS, RESPONSIVE');
	       		$modal.find('.description').text('Project 0');
	       		$modal.find('.button').on('click', function(){
	       			window.open('http://google.com');
	       		});

	       	}

	        break;
	    case 1:

	    	var arrayLength1 = array1.length;

	    	for(var i=0 ; i<arrayLength1 ; i++) {

	        	$modal.find('#roarCarousel').append('<div class="fade-in" data-src="' + array1[i] + '"></div>');
	       		$modal.find('.skills').text('CSS, HTML5, ANGULARJS, NODE JS, RAILS, RESPONSIVE');
	       		$modal.find('.description').text('Project 1');
	       		$modal.find('.button').on('click', function(){
	       			window.open('http://google.com');
	       		});

	       	}

	        break;
	    case 2:
	        
	    	var arrayLength2 = array2.length;

	    	for(var i=0 ; i<arrayLength2 ; i++) {

	        	$modal.find('#roarCarousel').append('<div class="fade-in" data-src="' + array2[i] + '"></div>');
	       		$modal.find('.skills').text('CSS, HTML5, ANGULARJS, NODE JS, RAILS, RESPONSIVE');
	       		$modal.find('.description').text('Project 2');
	       		$modal.find('.button').on('click', function(){
	       			window.open('http://google.com');
	       		});

	       	}

	        break;

	   	case 3:
	        
	    	var arrayLength3 = array3.length;

	    	for(var i=0 ; i<arrayLength3 ; i++) {

	        	$modal.find('#roarCarousel').append('<div class="fade-in" data-src="' + array3[i] + '"></div>');
	       		$modal.find('.skills').text('CSS, HTML5, ANGULARJS, NODE JS, RAILS, RESPONSIVE');
	       		$modal.find('.description').text('Project 3');
	       		$modal.find('.button').on('click', function(){
	       			window.open('http://google.com');
	       		});

	       	}

	        break;

	    case 4:
	        
	    	var arrayLength4 = array4.length;

	    	for(var i=0 ; i<arrayLength4 ; i++) {

	        	$modal.find('#roarCarousel').append('<div class="fade-in" data-src="' + array4[i] + '"></div>');
	       		$modal.find('.skills').text('CSS, HTML5, ANGULARJS, NODE JS, RAILS, RESPONSIVE');
	       		$modal.find('.description').text('Project 4');
	       		$modal.find('.button').on('click', function(){
	       			window.open('http://google.com');
	       		});

	       	}

	        break;

	    case 5:
	        
	    	var arrayLength5 = array5.length;

	    	for(var i=0 ; i<arrayLength5 ; i++) {

	        	$modal.find('#roarCarousel').append('<div class="fade-in" data-src="' + array5[i] + '"></div>');
	       		$modal.find('.skills').text('CSS, HTML5, ANGULARJS, NODE JS, RAILS, RESPONSIVE');
	       		$modal.find('.description').text('Project 5');
	       		$modal.find('.button').on('click', function(){
	       			window.open('http://google.com');
	       		});

	       	}

	        break;

	    case 6:
	        
	    	var arrayLength6 = array6.length;

	    	for(var i=0 ; i<arrayLength6 ; i++) {

	        	$modal.find('#roarCarousel').append('<div class="fade-in" data-src="' + array6[i] + '"></div>');
	       		$modal.find('.skills').text('CSS, HTML5, ANGULARJS, NODE JS, RAILS, RESPONSIVE');
	       		$modal.find('.description').text('Project 6');
	       		$modal.find('.button').on('click', function(){
	       			window.open('http://google.com');
	       		});

	       	}

	        break;
	}

	$('#firstModal').foundation('reveal', 'open');

});


$(document).on('opened.fndtn.reveal', '[data-reveal]', function (e) {


	if (e.namespace !== 'fndtn.reveal') {
        return;
    }

	var num = $('.fade-in').length;
  
	$('.fade-in').each(function(i) {
		console.log(i);
        var src = $(this).attr("data-src");
        if(i !== num-1) {

	        if (src) {
	            var img = new Image();
	            img.style.display = "none";
	            img.onload = function() {
	                $(this).fadeIn(500);
	            };
	            $(this).append(img);            
	            img.src = src;
	        } 

    	} else {

    		if (src) {
	            var img = new Image();
	            img.style.display = "none";
	            img.onload = function() {
	                $(this).fadeIn(500);
	            };
	            $(this).append(img);            
	            img.src = src;
	        } 

			$('#roarCarousel').slick({
			  dots: true,
			  infinite: false,
			  speed: 900,
			  slidesToShow: 1,
			  slidesToScroll: 1,
			  slidesToScroll: 1,
			  autoplay: true,
			  autoplaySpeed: 2000,
			  arrows: false,
			  responsive: [
			    {
			      breakpoint: 1024,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1,
			        infinite: false,
			        dots: true
			      }
			    },
			    {
			      breakpoint: 600,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    },
			    {
			      breakpoint: 480,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1
			      }
			    }
			  ]
			});
    	}
    });

});

$(document).on('closed.fndtn.reveal', '[data-reveal]', function (e) {
	if (e.namespace !== 'fndtn.reveal') {
	    return;
	}
  var modal = $(this);

  $('#roarCarousel').unslick();

  $(this).find('#roarCarousel').empty();
});


/**roar modal carousel**/

/**end roar modal carousel**/	

});